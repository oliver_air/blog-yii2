<?php

use yii\db\Migration;

/**
 * Handles the creation of table `category`.
 */
class m180108_133335_create_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('category', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'url'  => $this->string(255)->notNull(),
            'description'  => $this->text()->notNull(),
            'publish'  => $this->smallInteger(1)->notNull(),
            'date' => $this->dateTime()->notNull()
        ]);

        $this->createIndex(
            'idx-category-id',
            'category',
            'id'
        );

        $this->insert('category', [
            'name' => 'Спорт',
            'url' => 'sport',
            'description' => 'Про спорт',
            'publish' => 1
        ]);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('category');
    }
}
