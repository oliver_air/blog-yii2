<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pages`.
 */
class m180109_114828_create_pages_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('pages', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'url' => $this->string(255)->notNull(),
            'description' => $this->text()->notNull(),
            'publish' => $this->smallInteger(1)->notNull(),
        ]);

        $this->createIndex(
            'idx-pages-id',
            'pages',
            'url'
        );

        $this->insert('pages', [
            'name' => 'О сайте',
            'url' => 'about',
            'description' => 'О сайтер',
            'publish' => 1
        ]);

        $this->insert('pages', [
            'name' => 'Контакты',
            'url' => 'contact',
            'description' => 'Связь с нами',
            'publish' => 1
        ]);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('pages');
    }
}
