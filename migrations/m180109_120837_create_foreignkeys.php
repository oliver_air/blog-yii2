<?php

use yii\db\Migration;

/**
 * Class m180109_120837_create_foreignkeys
 */
class m180109_120837_create_foreignkeys extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $this->addForeignKey(
            'fk-post-id',
            'post',
            'category_id',
            'category',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-tagstoarticle-id',
            'tagstoarticle',
            'post_id',
            'post',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-tagstoarticle-id-tags',
            'tagstoarticle',
            'tag_id',
            'tags',
            'id',
            'CASCADE'
        );


    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180109_120837_create_foreignkeys cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180109_120837_create_foreignkeys cannot be reverted.\n";

        return false;
    }
    */
}
