<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tagstoarticle`.
 */
class m180109_113828_create_tagstoarticle_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('tagstoarticle', [
            'id' => $this->primaryKey(),
            'tag_id' => $this->integer()->notNull(),
            'post_id'  => $this->integer()->notNull(),
        ]);

        $this->createIndex(
            'idx-tagstoarticle-id',
            'tagstoarticle',
            'id'
        );

        $this->insert('tagstoarticle', [
        'tag_id' => 1,
        'post_id'  => 1,
    ]);

        $this->insert('tagstoarticle', [
            'tag_id' => 2,
            'post_id'  => 1,
        ]);

        $this->insert('tagstoarticle', [
            'tag_id' => 1,
            'post_id'  => 2,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('tagstoarticle');
    }
}
