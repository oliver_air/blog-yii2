<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post`.
 */
class m180108_134255_create_post_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('post', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'url' => $this->string(255)->notNull(),
            'description' => $this->text()->notNull(),
            'publish' => $this->smallInteger(1)->notNull(),
            'date' => $this->dateTime()->notNull(),
            'category_id' => $this->integer()->notNull(),
            'short_description' => $this->text()->notNull()
        ]);

        $this->createIndex(
            'idx-post-id',
            'post',
            'id'
        );

        $this->insert('post', [
            'name' => 'Статья 1',
            'url' => 'article-about-football',
            'description' => 'Текст статьи 1',
            'publish' => 1,
            'category_id' => 1,
            'short_description' => 'Анонс статьи 1'
        ]);

        $this->insert('post', [
            'name' => 'Статья 2',
            'url' => 'another-article-about-football',
            'description' => 'Текст статьи 2',
            'publish' => 1,
            'category_id' => 1,
            'short_description' => 'Анонс статьи 2'
        ]);
    }
    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('post');
    }
}
