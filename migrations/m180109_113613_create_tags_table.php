<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tags`.
 */
class m180109_113613_create_tags_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('tags', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'url' => $this->string(255)->notNull(),
        ]);

        $this->createIndex(
            'idx-tags-id',
            'tags',
            'url'
        );

        $this->insert('tags', [
        'name' => 'Спорт',
        'url' => 'sport',
        ]);

        $this->insert('tags', [
            'name' => 'Футбол',
            'url' => 'football',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('tags');
    }
}
