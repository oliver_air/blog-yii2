<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post".
 *
 * @property int $id
 * @property string $name
 * @property string $url
 * @property string $description
 * @property int $publish
 * @property string $date
 * @property int $category_id
 * @property string $short_description
 *
 * @property Category $category
 * @property Tagstoarticle[] $tagstoarticles
 */
class Post extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'url', 'description', 'publish', 'date', 'category_id', 'short_description'], 'required'],
            [['description', 'short_description'], 'string'],
            [['publish', 'category_id'], 'integer'],
            [['date'], 'safe'],
            [['name', 'url'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'url' => 'Url',
            'description' => 'Description',
            'publish' => 'Publish',
            'date' => 'Date',
            'category_id' => 'Category ID',
            'short_description' => 'Short Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTagstoarticles()
    {
        return $this->hasMany(Tagstoarticle::className(), ['post_id' => 'id']);
    }
}
