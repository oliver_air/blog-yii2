<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
$this->title = 'Новости';
?>
<div class="site-index">

    <div class="col-lg-10">
        <div class="page-header">
            <h1><?=Yii::t('app', 'Новости'); ?></h1>
        </div>
        <?php foreach($posts as $post){ ?>
            <div class="col-lg-12 text-left">
                <h3><?=$post->name; ?>  <small><?=$post->date; ?></small></h3>
                <div class="container">
                    <?=$post->short_description; ?>
                </div>
                <div class="col-lg-6">  <?=Yii::t('app', 'Категория'); ?>: <?= Html::a($post->category->name, ['/'.$post->category->url], ['class' => 'category-link']) ?></div>
                <div class="col-lg-6 text-right"><?= Html::a(Yii::t('app', 'Читать полностью'), ['/'.$post->category->url.'/'.$post->url], ['class' => 'btn-sm']) ?></div>
            </div>
        <?php } ?>
    </div>
    <div class="col-lg-2">
        <div class="category">
            <div class="page-header">
                <?=Yii::t('app', 'Категории'); ?>
            </div>
                <ul>
                    <?php foreach ($categories as $category) { ?>
                        <li>
                            <?= Html::a($category->name, ['/'.$category->url], ['class' => 'category-link']) ?>
                        </li>
                    <?php } ?>
                </ul>
        </div>

        <div class="category">
            <div class="page-header">
                <?=Yii::t('app', 'Тэги'); ?>
            </div>
                <?php foreach ($tags as $tag) { ?>
                        <?= Html::a($tag->name, ['/tags/'.$tag->url], ['class' => 'tag-link btn-sm']) ?>
                <?php } ?>
        </div>

    </div>
</div>
